import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    comidas:[
      //guardamos en un array la info q necesitamos [ ]
      {name:'chocolate',cantidad:'0'},
      {name:'paneton',cantidad:'0'},
      {name:'agua',cantidad:'0'},
      {name:'limon',cantidad:'0'},
     ]
  },
  mutations: {
    aumentar(state, index){
      //vamos a detectar a los elemntos del arreglo por el indice
      state.comidas[index].cantidad++
    },
    reiniciar(state){
      state.comidas.forEach(element => {
        element.cantidad = 0

      })
    }

  },
  actions: {

  }
})
